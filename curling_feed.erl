-module(curling_feed).
-behavior(gen_event).
-export([init/1,
	     handle_event/2,
	     handle_call/2,
	     handle_info/2,
	     terminate/2,
	     code_change/3]).


init([Pid]) ->
	{ok, Pid}.

handle_event(Msg, Pid) ->
	Pid ! {curling_feed, Msg},
	{ok, Pid}.

handle_call(_, State) ->
	{ok, State}.

handle_info(_, State) ->
	{ok, State}.

terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.