-module(curling).
-export([start_link/2,
	     set_teams/3,
	     add_point/3,
	     next_round/1,
	     terminate/1,
	     join_feed/2,
	     leave_feed/2,
	     game_info/1]).


start_link(TeamA, TeamB) ->
	{_, Pid} = gen_event:start_link(),
	gen_event:add_handler(Pid, curling_scoreboard, []),
	gen_event:add_handler(Pid, curling_accumulator, []),
	set_teams(Pid, TeamA, TeamB),
	{ok, Pid}.

set_teams(Pid, TeamA, TeamB) ->
	gen_event:notify(Pid, {set_teams, TeamA, TeamB}),
	ok.

add_point(Pid, Team, Score) ->
	gen_event:notify(Pid, {add_points, Team, Score}),
	ok.

next_round(Pid) ->
	gen_event:notify(Pid, next_round),
	ok.

terminate(Pid) ->
	gen_event:delete_handler(Pid, curling_scoreboard, turn_off),
	ok.

join_feed(Pid, ToPid) ->
	HandlerId = {curling_feed, make_ref()},
	gen_event:add_handler(Pid, HandlerId, [ToPid]),
	HandlerId.

leave_feed(Pid, HandlerId) ->
	gen_event:delete_handler(Pid, HandlerId, leave_feed),
	ok.

game_info(Pid) ->
	gen_event:call(Pid, curling_accumulator, game_data).