-module(curling_scoreboard_hw).
-export([set_teams/2, add_point/1, next_round/0, reset_board/0]).

set_teams(TeamA, TeamB) ->
	io:format("Ход игры: Команда ~s против команды ~s~n", [TeamA, TeamB]).

add_point(Team) ->
	io:format("Ход игры: Очки команды ~s увеличены на 1~n", [Team]).

next_round() ->
	io:format("Ход игры: конец раунда~n").

reset_board() ->
	io:format("Ход игры: Все команды сброшены и очки обнулены~n").
